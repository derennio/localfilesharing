package dev.ennio.gui;

import dev.ennio.business.IFileService;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * @author ennio
 **/
public class GuiHelper extends Component
{
    /** Window properties. **/
    private final int Width;
    private final int Height;

    /** FileService instance in order to perform operations. **/
    private final IFileService FileService;

    /**
     * Java Swing fields that need to be updated
     * or accessed during from another scope runtime.
     **/
    private JLabel StatusLabel;
    private JButton Confirm;
    private JTextField ReceivingFileName, LocalCode;

    /** Fields required for file transfers. **/
    private ActionType CurrentAction;
    private File SelectedFile;

    public GuiHelper(IFileService fileService)
    {
        this.Width = 400;
        this.Height = 400;
        this.FileService = fileService;
    }

    /**
     * Method for setting up the main frame and handling
     * any kind of user interaction.
     */
    public void setupFrame()
    {
        JFrame mainFrame = new JFrame("FileSharing");

        JLabel infoLabel = new JLabel("<html><body>Bitte wähle, was Du tun willst.<br>" +
                "Dein Code für Dateiübertragungen: " + getLocalCode() + "</body></html>");
        infoLabel.setBounds(100, 10, 250, 30);

        this.StatusLabel = new JLabel("");
        this.StatusLabel.setBounds(100, 35, 200, 30);

        this.Confirm = new JButton("Bestätigen");
        this.Confirm.setBounds(100, 300, 170, 30);
        this.Confirm.addActionListener(event -> submit());
        this.Confirm.setEnabled(false);

        this.ReceivingFileName = new JTextField("Dateiname.ext");
        this.ReceivingFileName.setBounds(100, 200, 170, 30);
        this.ReceivingFileName.setEnabled(false);

        this.LocalCode = new JTextField("100");
        this.LocalCode.setBounds(100, 250, 80, 30);
        this.LocalCode.setEnabled(false);

        JButton clientSelect = new JButton("Datei herunterladen");
        clientSelect.setBounds(100, 150, 170, 30);
        clientSelect.addActionListener(event -> {
            this.CurrentAction = ActionType.DOWNLOAD;

            // update components
            this.Confirm.setEnabled(true);
            this.ReceivingFileName.setEnabled(true);
            this.LocalCode.setEnabled(true);
        });

        JFileChooser fileChooser = new JFileChooser();

        JButton serverSelect = new JButton("Datei veröffentlichen");
        serverSelect.setBounds(100, 100, 170, 30);
        serverSelect.addActionListener(event ->
        {
            int returnValue = fileChooser.showOpenDialog(GuiHelper.this);

            if (returnValue == JFileChooser.APPROVE_OPTION)
            {
                File file = fileChooser.getSelectedFile();

                // update components
                this.Confirm.setEnabled(true);
                this.ReceivingFileName.setEnabled(false);
                this.LocalCode.setEnabled(false);

                // prepare upload
                this.SelectedFile = file;
                this.CurrentAction = ActionType.UPLOAD;
                this.StatusLabel.setText("Datei hochladen: " + file.getName());
            }
        });

        mainFrame.add(this.StatusLabel);
        mainFrame.add(this.Confirm);
        mainFrame.add(this.ReceivingFileName);
        mainFrame.add(this.LocalCode);
        mainFrame.add(infoLabel);
        mainFrame.add(serverSelect);
        mainFrame.add(clientSelect);

        mainFrame.setSize(this.Width, this.Height);
        mainFrame.setLayout(null);
        mainFrame.setVisible(true);
        mainFrame.setResizable(false);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Method for calling service in
     * order to perform according action.
     **/
    private void submit()
    {
        if (this.CurrentAction == null)
        {
            throw new IllegalArgumentException("currentAction");
        }

        if (this.CurrentAction == ActionType.UPLOAD)
        {
            this.StatusLabel.setText("Datei hochgeladen!");
            this.Confirm.setEnabled(false);
            if (this.FileService.upload(this.SelectedFile))
            {
                this.SelectedFile = null;
                this.StatusLabel.setText("");
            }
        }
        else if (this.CurrentAction == ActionType.DOWNLOAD)
        {
            this.StatusLabel.setText("Versuche, Datei herunterzuladen!");
            this.Confirm.setEnabled(false);
            if (this.FileService.download(this.ReceivingFileName.getText(), this.LocalCode.getText()))
            {
                this.LocalCode.setEnabled(false);
                this.ReceivingFileName.setEnabled(false);
                this.ReceivingFileName.setText("Dateiname.ext");
                this.StatusLabel.setText("");
            }
        }
    }

    /**
     * Helper method to retrieve the local code to share with the connection partner.
     * @return last three digits of the local ip address.
     */
    private String getLocalCode()
    {
        try
        {
            String host = Inet4Address.getLocalHost().getHostAddress();
            return host.split("\\.")[3];
        } catch (UnknownHostException e)
        {
            e.printStackTrace();
        }
        return "ERROR";
    }

    enum ActionType
    {
        UPLOAD, DOWNLOAD
    }
}
