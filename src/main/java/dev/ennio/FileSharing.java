package dev.ennio;

import dev.ennio.business.FileService;
import dev.ennio.gui.GuiHelper;

/**
 * @author ennio
 **/
public class FileSharing
{
    public static void main(String[] args)
    {
        new GuiHelper(new FileService()).setupFrame();
    }
}
