package dev.ennio.business;

import java.io.File;

/**
 * @author ennio
 **/
public interface IFileService
{
    /**
     * Exposes a file to the local network.
     * @param file target to be uploaded.
     */
    boolean upload(File file);

    /**
     * Downloads a file from the local network.
     * @param fileName the name that the downloaded file will be called.
     * @param localCode last three digits of target IPv4 address.
     */
    boolean download(String fileName, String localCode);
}
