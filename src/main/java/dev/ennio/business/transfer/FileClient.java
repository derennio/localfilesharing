package dev.ennio.business.transfer;

import java.io.*;
import java.net.Inet4Address;
import java.net.Socket;

/**
 * @author ennio
 **/
public class FileClient implements IFileTransfer
{
    private final int BufferSize;
    private final int Port;
    private final String FileName, LocalCode;

    public FileClient(int bufferSize, int port, String localCode, String fileName)
    {
        this.BufferSize = bufferSize;
        this.Port = port;
        this.LocalCode = localCode;
        this.FileName = fileName;
    }

    /**
     * Connects to the ServerSocket on the local network and downloads the destined file.
     * @return whether the file transfer has been executed successfully.
     */
    @Override
    public Boolean call() {
        try
        {
            String ipv4 = Inet4Address.getLocalHost().getHostAddress();
            String baseAddress = ipv4.substring(0, ipv4.length() - 3);

            Socket socket = new Socket(baseAddress + this.LocalCode, this.Port);

            InputStream inputStream = socket.getInputStream();

            String homeDir = System.getProperty("user.home");
            OutputStream outputStream = new FileOutputStream(homeDir + "/Downloads/" + this.FileName);

            byte[] fileBuffer = new byte[this.BufferSize];
            int bytesRead;
            while ((bytesRead = inputStream.read(fileBuffer)) > 0)
            {
                outputStream.write(fileBuffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();
            socket.close();
            return true;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
}
