package dev.ennio.business.transfer;

import java.util.concurrent.Callable;

/**
 * @author ennio
 * @implNote only required for call from within service
 **/
public interface IFileTransfer extends Callable<Boolean> {
}
