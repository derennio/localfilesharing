package dev.ennio.business.transfer;

import java.io.*;
import java.net.*;

/**
 * @author ennio
 **/
public class FileServer implements IFileTransfer
{
    private final int BufferSize;
    private final int Port;
    private final String FileName;

    public FileServer(int bufferSize, int port, String fileName)
    {
        this.BufferSize = bufferSize;
        this.Port = port;
        this.FileName = fileName;
    }

    /**
     * Opens a ServerSocket on the local network and provides the destined file.
     * @return whether the file transfer has been executed successfully.
     */
    @Override
    public Boolean call() {
        try
        {
            ServerSocket serverSocket = new ServerSocket(this.Port);
            Socket socket = serverSocket.accept();

            InputStream inputStream = new FileInputStream(this.FileName);
            OutputStream outputStream = socket.getOutputStream();

            byte[] fileBuffer = new byte[this.BufferSize];
            int bytesRead;

            while ((bytesRead = inputStream.read(fileBuffer)) > 0) {
                outputStream.write(fileBuffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();
            socket.close();
            return true;
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
        return false;
    }
}
