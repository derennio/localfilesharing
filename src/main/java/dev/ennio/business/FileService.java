package dev.ennio.business;

import dev.ennio.business.transfer.FileClient;
import dev.ennio.business.transfer.FileServer;
import dev.ennio.business.transfer.IFileTransfer;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author ennio
 **/
public class FileService implements IFileService
{
    /**
     * Exposes a file to the local network.
     * @param file target to be uploaded.
     */
    @Override
    public boolean upload(File file)
    {
        FileServer server = new FileServer(16 * 1024, 25565, file.getAbsolutePath());

        return runFuture(server);
    }

    /**
     * Downloads a file from the local network.
     * @param fileName the name that the downloaded file will be called.
     * @param localCode last three digits of target IPv4 address.
     */
    @Override
    public boolean download(String fileName, String localCode)
    {
        FileClient client = new FileClient(16 * 1024, 25565, localCode, fileName);

        return runFuture(client);
    }

    /**
     * Helper method in order to run FutureTasks related to a file transfer.
     * @param transfer file transfer instance that will be executed.
     * @see FutureTask asynchronous computation.
     * @see IFileTransfer interface for file transfer operations.
     **/
    private boolean runFuture(IFileTransfer transfer)
    {
        FutureTask<Boolean> future = new FutureTask<>(transfer);
        future.run();

        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }
}
